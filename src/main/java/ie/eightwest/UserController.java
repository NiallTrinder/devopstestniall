package ie.eightwest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class UserController {

    @Autowired
    UserDao udb;

    @CrossOrigin(origins="*")
    @RequestMapping(method= RequestMethod.GET, value="users/{id}", produces = "application/json")
    public User getUser(@PathVariable long id) {

        User user = null;
        try {
            user = udb.getUser(id);

            if (user==null) {
                throw new UserNotFoundException();
            }
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        return user;
    }

    @CrossOrigin(origins="*")
    @RequestMapping(method=RequestMethod.GET, value="users", produces = "application/json")
    public Collection<User> getUsers(){

        Collection<User> users = udb.getUsers();

        return users;

    }

    @CrossOrigin(origins="*")
    @RequestMapping(method=RequestMethod.DELETE, value="users/{id}", produces = "application/json")
    public void delete(@PathVariable long id) {
        try {
            udb.deleteUser(id);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
    }

    @CrossOrigin(origins="*")
    @RequestMapping(method=RequestMethod.POST, value="users", produces = "application/json")
    public User addUser(@RequestBody User user) {

        User newUser;

        try {
            newUser = udb.addUser(user);
        } catch (UserDaoException e) {
            throw new UserNotFoundException();
        }

        return newUser;
    }


    @CrossOrigin(origins="*")
    @RequestMapping(method=RequestMethod.PUT, value="users/{id}", produces = "application/json")
    public User updateUser(@RequestBody User user, @PathVariable long id){

        // check that id == user.getId();
        User updatedUser = null;
        try {
            updatedUser = udb.updateUser(user);
        } catch (UserDaoException e) {
            throw new UserNotFoundException();
        }

        return updatedUser;
    }
}
