package ie.eightwest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserManager {

    @Autowired
    private UserDao udb;

    Collection<User> getUsers() {
        return udb.getUsers();
    }

}
