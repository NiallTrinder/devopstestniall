package ie.eightwest;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

public class UserDaoInMemory implements UserDao{

    protected HashMap<Long, User> userMap;

    public UserDaoInMemory() {
        userMap = new HashMap<>();
    }

    public User addUser(User user) throws UserDaoException{
        // TBD - assign a new id
        if(user.getId() == -1) {
            // assign new id and add to map
            Set<Long> keys = userMap.keySet();
            long lastId = keys.stream()
                    .max(Comparator.comparing(Long::valueOf))
                    .get();
            user.setId(lastId + 1);
        } else {
            if(userMap.containsKey(user.getId())) {
                // id already exists, throw exception
                throw new UserDaoException("User id already exists.");
            }
        }
        userMap.put(user.getId(), user);
        return user;
    }

    public Collection<User> getUsers() {
        return userMap.values();
    }

    public User getUser(long id) throws UserDaoException{
        if (userMap.containsKey(id)) {
            return userMap.get(id);
        } else{
            throw new UserDaoException("Exception Thrown");
        }
    }

    @Override
    public void close() throws UserDaoException {
        userMap.clear();
    }

    public void deleteUser(long id) throws UserDaoException{
        if (userMap.containsKey(id)) {
            userMap.remove(id);
        } else{
            throw new UserDaoException("Cannot Delete User that does not exist.");
        }
    }

    public User updateUser(User user) throws UserDaoException{
        if (userMap.containsKey(user.id)) {
            userMap.replace(user.getId(), user);
            return user;
        } else {
            throw new UserDaoException("Cannot update user if user does not exist.");
        }
    }

    @Override
    public String toString() {
        return userMap.toString();
    }

    public static void main(String[] args) {
        // create an interface rather than a specific implementation
        // UserDaoInMemory udb = new UserDaoInMemory;
        UserDao udb = new UserDaoInMemory();
        User user1 = new User(1,"Niall", "niall@email.com", true);
        User user2 = new User(2,"John", "john@email.com", true);
        try {
            udb.addUser(user1);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            udb.addUser(user2);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(udb);
        Collection<User> users = udb.getUsers();
        for (User u : users) {
            System.out.println(u.getName());
        }

        User found = null;
        try {
            found = udb.getUser(1);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        User notFound = null;
        try {
            notFound = udb.getUser(99);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(found);
        System.out.println(notFound);

        System.out.println("=======================");
        try {
            System.out.println(udb.getUser(2));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            udb.deleteUser(2);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(udb.getUser(2));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println("=======================");
        try {
            udb.addUser(user2);
            udb.addUser(user2);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        User user3 = new User(2, "Homer", "Homer@email.com", false);
        try {
            udb.updateUser(user3);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(udb.getUser(2));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        User userToUpdate = new User(3, "update", "update.email", true);
        try {
            udb.updateUser(userToUpdate);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println("===============================");
        User n1 = new User("new", "new@email.com", false);
        User n2 = new User(99, "another new user", "new13123@email.com", true);
        try {
            udb.addUser(n1);
            udb.addUser(n2);
            users = udb.getUsers();
            System.out.println(users);
            System.out.println(n1);
            System.out.println(n2);

            User n3 = new User(2, "yet another new user", "email.com", true);
            udb.addUser(n3);
            System.out.println(users);
            System.out.println(n3);


        } catch (UserDaoException e) {
            e.printStackTrace();
        }
    }
}
