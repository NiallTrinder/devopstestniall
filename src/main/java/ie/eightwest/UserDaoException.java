package ie.eightwest;

public class UserDaoException extends Exception{
    public UserDaoException(String message) {
        super(message);
    }
}
