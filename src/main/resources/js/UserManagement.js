class User{
    constructor(id, name, email, active){
        this.id = id;
        this.name = name;
        this.email = email;
        this.active = active;
    }
}

class UserDao {

    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    getAllUsers() {
        return $.ajax({
            method: "GET",
            url: baseUrl,
        })
    };


        /*var p = new Promise(function(accept,reject) {
            $.ajax({
                method: "GET",
                url: baseUrl,
                success: function(users) {
                    accept(users);
                },
                error: function(error) {
                    reject(error);
                }
            });
            return p;
        });*/


    getAllUsersOriginal(onOk) {
        $.ajax({
            method: "GET",
            url: baseUrl,
            success: function(users) {
                    onOk(users);   //this wont work
            },
            error: function(error) {
                alert(error);
            }
        });
    }

    getUser(id) {
        return $.ajax({
            method: "GET",
            url: `${baseUrl}/${id}`,
        })
    }
    deleteUser(id) {
        return $.ajax({
            method: "DELETE",
            url: `${baseUrl}/${id}`,
        })
    }

    addUser(user) {
        return $.ajax({
            method: "POST",
            url: `${baseUrl}`,
            data: JSON.stringify(user),
            contentType: "application/json"
        })
    }

    updateUser(user) {
        return $.ajax({
            method: "PUT",
            url: `${baseUrl}/${id}`,
            data: JSON.stringify(user),
            contentType: "application/json"
        })
    }
}