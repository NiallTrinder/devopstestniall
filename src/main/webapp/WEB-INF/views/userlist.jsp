<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>User List Page</title>
</head>
<body>
<select>
<c:forEach items="${users}" var="element">
    <option value="${element.getId()}">${element.getName()}</option>
</c:forEach>
</select>
<ul>
    <c:forEach items="${users}" var="element">
        <li>
            <a href="userdetail/${element.getId()}">${element.getName()}</a>
        </li>
    </c:forEach>
</ul>
</body>
</html>
