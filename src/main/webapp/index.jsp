<%@page language="java"%>
<%@page import="ie.eightwest.DevopsIntro"%>

<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
    </head>
<body>
<h2>Hello World!</h2>

<a href="home">Home Page</a>
<a href="static/static.html">Static Page</a>
<a href="users">Users Page</a>

<div>
    a change was made.
</div>

<%
    out.print(DevopsIntro.message);
%>

</body>

<script>
    $(document).ready(function(){
        setTimeout(function() {
            $('body').append("<button>Press Me</button>");
        }, 5000);
    });

</script>

</html>
