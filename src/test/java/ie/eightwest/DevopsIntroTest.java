package ie.eightwest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DevopsIntroTest {

    @Before
    public void setup() {}

    @After
    public void tearDown() {}

    @Test
    public void correctMessage() {
        Assert.assertEquals(DevopsIntro.message, "deployed by me in tomcat");
    }

    @Test
    public void pass99in100() {

        if(Math.random() > 0.991) {
            Assert.fail("fails 1 in 100.");
        }
        else {
            Assert.assertTrue(true);
        }
    }
}
