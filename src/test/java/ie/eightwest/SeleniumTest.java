package ie.eightwest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {

/*    // setup -> point to chromedriver location
    System.setProperty("webdriver.chrome.driver", "f:\\JavaFS\\drivers\\chromedriver_win32\\chromedriver.exe");

    // set selenium to run headless
    ChromeOptions co = new ChromeOptions();
    co.addArguments("headless");

    ChromeDriver cd = new ChromeDriver();

    @Before
    public void setUp() {

        // load url
        cd.get("http://localhost:8080/niall/");
    }

    @After
    public void tearDown() {
        cd.quit();
    }*/

    @Test
    public void seleniumTest() {
        // setup -> point to chromedriver location
        //System.setProperty("webdriver.chrome.driver", "f:\\JavaFS\\drivers\\chromedriver_win32\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "c:\\chromedriver\\chromedriver_win32\\chromedriver.exe");

        // set selenium to run headless if you want
        ChromeOptions co = new ChromeOptions();
        co.addArguments("headless");

        ChromeDriver cd = new ChromeDriver();

        // load url
        cd.get("http://168.63.36.18:8080/niall/");

        WebDriverWait wait = new WebDriverWait(cd, 10);

        WebElement btn = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.tagName("button"))
        );

        Assert.assertNotNull(btn);

        WebElement e = cd.findElementByLinkText("Home Page");
        e.click();
        WebElement h1 = cd.findElementByTagName("h1");
        String actual = h1.getText();
        String expected = "THIS IS THE HOME PAGE!";
        Assert.assertEquals(actual, expected);



        // close the webpages after
        cd.quit();
    }

    @Test
    public void openHomePage() {

    }

    @Test
    public void openStaticPage() {

    }

    @Test
    public void openUsersPage() {

    }
}
